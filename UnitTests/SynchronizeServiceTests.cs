﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SynchronizeService.Common;
using SynchronizeService.Models;
using SynchronizeService.Models.Enums;
using SynchronizeService.Services.Interfaces;

namespace UnitTests
{

    [TestFixture]
    public class SynchronizeServiceTests
    {

        private Mock<ISynchonizationStatusManager> _synchonizationStatusManager;
        private Mock<IFeedDbService> _syncFeedDbService;

        private List<SynchonizationStatus> _synchonizationStatusList;

        private SynchronizeService.SynchronizeService _synService;

        [SetUp]
        public void SetUp()
        {
            _synchonizationStatusList = new List<SynchonizationStatus>();


            _synchonizationStatusManager = new Mock<ISynchonizationStatusManager>();

            _synchonizationStatusManager.Setup(s => s.Success(It.IsAny<SynchonizationStatus>())).Callback((SynchonizationStatus feedInfo) => { _synchonizationStatusList.Add(feedInfo); });

            _syncFeedDbService = new Mock<IFeedDbService>();
            _syncFeedDbService.Setup(s => s.GetChunkFromDb<FeedVillaModel>(It.IsAny<SyncFeedRequest>())).Returns(new SyncFeedPageModel<FeedVillaModel> { Items = new List<FeedVillaModel> { new FeedVillaModel() } });

            _synService = new SynchronizeService.SynchronizeService(Mock.Of<ILogger>(), Mock.Of<IFeedStorage>(), _synchonizationStatusManager.Object, _syncFeedDbService.Object);
        }

        [Test]
        public async Task ShouldStartImportFromLastSuccessImportTime()
        {
            //ARRANGE
            _synchonizationStatusManager.Setup(s => s.GetLast(It.IsAny<FeedSyncInfoType>(), It.Is<FeedSyncInfoStatus[]>(c => c.Contains(FeedSyncInfoStatus.Success)))).Returns(new SynchonizationStatus { UpdateBeginTime = DateTime.Parse("02/Jan/2017"), ItemsProcessed = 100 });
            _synchonizationStatusManager.Setup(s => s.New(It.IsAny<SynchonizationStatus>())).Returns(new SynchonizationStatus { Status = FeedSyncInfoStatus.Started, UpdateBeginTime = DateTime.Parse("03/Jan/2017") });

            //ACT
            await _synService.Synchronize(FeedSyncInfoType.Properties);

            //ASSERT
            _synchonizationStatusManager.Verify(x => x.Update(It.IsAny<SynchonizationStatus>()), Times.Exactly(1));
            _synchonizationStatusManager.Verify(x => x.Success(It.IsAny<SynchonizationStatus>()), Times.Exactly(1));
            Assert.AreEqual(1, _synchonizationStatusList.First().ItemsProcessed);
        }

        [Test]
        public async Task ShouldTookIntoAccountAlreadyProcessedItemsWhenRetry()
        {
            //ARRANGE
            _synchonizationStatusManager.Setup(s => s.GetLast(It.IsAny<FeedSyncInfoType>(), It.Is<FeedSyncInfoStatus[]>(c => c.Contains(FeedSyncInfoStatus.Faild)))).Returns(new SynchonizationStatus { Status = FeedSyncInfoStatus.Faild, UpdateBeginTime = DateTime.Parse("03/Jan/2017"), ItemsProcessed = 10, LastPageToken = "_2" });
            _synchonizationStatusManager.Setup(s => s.New(It.IsAny<SynchonizationStatus>())).Returns(new SynchonizationStatus { Status = FeedSyncInfoStatus.Started, UpdateBeginTime = DateTime.Parse("03/Jan/2017"), ItemsProcessed = 10 });
            _syncFeedDbService.Setup(s => s.GetChunkFromDb<FeedVillaModel>(It.IsAny<SyncFeedRequest>())).Returns<SyncFeedRequest>(
                (sr) => new SyncFeedPageModel<FeedVillaModel>
                {
                    Items = sr.PageToken == "_2" ? new List<FeedVillaModel> { new FeedVillaModel() } : new List<FeedVillaModel> { new FeedVillaModel(), new FeedVillaModel() }
                });

            //ACT
            await _synService.Synchronize(FeedSyncInfoType.Properties);

            //ASSERT
            _synchonizationStatusManager.Verify(x => x.Update(It.IsAny<SynchonizationStatus>()), Times.Exactly(1));
            _synchonizationStatusManager.Verify(x => x.Success(It.IsAny<SynchonizationStatus>()), Times.Exactly(1));
            Assert.AreEqual(11, _synchonizationStatusList.First().ItemsProcessed);
        }

        [Test]
        public async Task ShouldNotStartNewJobIfPreviousStillRunning()
        {
            //ARRANGE
            var feedCachedStorageMock = new Mock<IFeedStorage>();
            feedCachedStorageMock.Setup(s => s.UpdateStorage(It.IsAny<List<FeedVillaModel>>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(async () =>
            {
                await Task.Delay(2000);
            });
            _synchonizationStatusManager.Setup(s => s.GetLast(It.IsAny<FeedSyncInfoType>(), It.Is<FeedSyncInfoStatus[]>(c => c.Contains(FeedSyncInfoStatus.Success)))).Returns(new SynchonizationStatus { UpdateBeginTime = DateTime.Parse("02/Jan/2017"), ItemsProcessed = 100 });
            _synchonizationStatusManager.Setup(s => s.New(It.IsAny<SynchonizationStatus>())).Returns(new SynchonizationStatus { Status = FeedSyncInfoStatus.Started, UpdateBeginTime = DateTime.Parse("03/Jan/2017") });
            var syncService1 = new SynchronizeService.SynchronizeService(Mock.Of<ILogger>(), feedCachedStorageMock.Object, _synchonizationStatusManager.Object, _syncFeedDbService.Object);
            var syncService2 = new SynchronizeService.SynchronizeService(Mock.Of<ILogger>(), Mock.Of<IFeedStorage>(), _synchonizationStatusManager.Object, _syncFeedDbService.Object);

            //ACT
            var process1 = syncService1.Synchronize(FeedSyncInfoType.Properties);
            var process2 = syncService2.Synchronize(FeedSyncInfoType.Properties);
            await Task.WhenAll(process1, process2);

            //ASSERT
            _synchonizationStatusManager.Verify(x => x.Success(It.IsAny<SynchonizationStatus>()), Times.Exactly(1));
        }
    }
}
