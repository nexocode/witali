namespace SynchronizeService.Models.Enums
{
    /// <summary>
    /// Type of entries in DB
    /// </summary>
    public enum FeedSyncInfoType
    {
        /// <summary>
        /// Property details
        /// </summary>
        Properties,
        /// <summary>
        /// Information regarding rental availability
        /// </summary>
        Availabilities,
        /// <summary>
        /// Removed properties
        /// </summary>
        DisabledProperties
    }
}