﻿using System;
using SynchronizeService.Models.Enums;

namespace SynchronizeService.Models
{
    public class SynchonizationStatus
    {
        public int Id { get; set; }

        public DateTime UpdateBeginTime { get; set; }

        public DateTime? UpdateEndTime { get; set; }

        public FeedSyncInfoType Type { get; set; }

        public FeedSyncInfoStatus Status { get; set; }

        public int ItemsProcessed { get; set; }

        public string LastPageToken { get; set; }
    }
}