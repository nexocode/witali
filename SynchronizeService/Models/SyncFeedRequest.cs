using System;
using SynchronizeService.Models.Enums;

namespace SynchronizeService.Models
{
    public class SyncFeedRequest
    {
        public const int NumberOfItemsForSingleChunkFromDb = 100;

        public SyncFeedRequest(SynchonizationStatus synchonizationStatus)
        {
            IsInitial = synchonizationStatus.UpdateBeginTime == DateTime.Parse("1970-01-01 00:00:00.000");
            FeedFromDate = synchonizationStatus.UpdateBeginTime;
            MaxResults = NumberOfItemsForSingleChunkFromDb;
            PageToken = synchonizationStatus.Status == FeedSyncInfoStatus.Success
                ? null
                : synchonizationStatus.LastPageToken;
        }

        /// <summary>
        /// Number of entries per single chunk
        /// </summary>
        public int MaxResults { get; set; }

        /// <summary>
        /// Token for current page
        /// 
        /// NULL in case of first page
        /// </summary>
        public string PageToken { get; set; }

        /// <summary>
        /// Date of last synchornization
        /// Base on that date will be determined entries which should be synchronized
        /// 
        /// In case of first synchonization date date should be set to 1970-01-01 00:00:00.000
        /// </summary>
        public DateTime FeedFromDate { get; set; }

        /// <summary>
        /// If True, then all entries will be synchonized 
        /// Default: False
        /// </summary>
        public bool IsInitial { get; set; }
    }
}