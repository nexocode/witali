﻿using System;

namespace SynchronizeService.Models
{
    public class FeedAvailabilityDiaryModel : FeedDocument
    {
        public DateTime DatesStartFrom { get; set; }
        public string Dates { get; set; }
    }
}