﻿using System;

namespace SynchronizeService.Models
{
    public class FeedDocument
    {
        public bool SerilazizeSpecFields;

        public string Id { get; set; }

        public int VendorId { get; set; }

        public int SupplierId { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime? DateUpdated { get; set; }
    }
}