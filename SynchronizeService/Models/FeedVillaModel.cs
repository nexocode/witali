﻿namespace SynchronizeService.Models
{
    public class FeedVillaModel : FeedDocument
    {
        public string PropertyName { get; set; }

        public string Description { get; set; }

        public bool Disabled { get; set; }
    }
}