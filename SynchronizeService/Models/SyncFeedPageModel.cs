using System.Collections.Generic;

namespace SynchronizeService.Models
{
    public class SyncFeedPageModel<T> where T : FeedDocument
    {
        /// <summary>
        /// List of entries
        /// </summary>
        public List<T> Items { get; set; }

        /// <summary>
        /// Token for next chunk of data 
        /// 
        /// NULL in case of last chunk
        /// </summary>
        public string NextPageToken { get; set; }
    }
}