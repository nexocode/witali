﻿namespace SynchronizeService.Common
{
    /// <summary>
    /// Example implementation of logger. In production environment could be used any known tool for logging, e.g. log4net
    /// </summary>
    public interface ILogger
    {
        void Info(string message);

        void Error(string message);
    }

    public class Logger : ILogger
    {
        public void Info(string message)
        {
            //loging log on INFO level
        }

        public void Error(string message)
        {
            //loging log on ERROR level
        }
    }
}
