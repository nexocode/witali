using System;
using System.Collections.Concurrent;
using SynchronizeService.Models.Enums;

namespace SynchronizeService.Common
{
    public class SyncFeedLock
    {
        private readonly ConcurrentDictionary<FeedSyncInfoType, bool> _runningJobs = new ConcurrentDictionary<FeedSyncInfoType, bool>();

        public bool TryLock(FeedSyncInfoType type)
        {
            return _runningJobs.TryAdd(type, true);
        }

        public void Release(FeedSyncInfoType type)
        {
            var isRemoved = false;
            _runningJobs.TryRemove(type, out isRemoved);
            if (!isRemoved)
            {
                throw new InvalidOperationException($"Can't release sync feed job for type: {type}");
            }
        }
    }
}