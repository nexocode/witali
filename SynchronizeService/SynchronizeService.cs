﻿using System;
using System.Threading.Tasks;
using SynchronizeService.Common;
using SynchronizeService.Models;
using SynchronizeService.Models.Enums;
using SynchronizeService.Services.Interfaces;

namespace SynchronizeService
{
    /// <summary>
    /// Service for synchronization secondary storage with main database  
    /// 
    /// ISyncFeedDbService is responsible for fetching data from main DB
    /// 
    /// IFeedCachedStorage is responsible for pesisting data in secondary storage
    /// 
    /// ISynchonizationStatusManager is responsible for providing necessary information regarding previous and current synchronizations
    /// </summary>
    public class SynchronizeService : IDisposable
    {
        private ILogger Logger { get; set; }
        private readonly IFeedStorage _feedStorage;
        private readonly ISynchonizationStatusManager _synchonizationStatusManager;
        private readonly IFeedDbService _feedDbService;
        private static readonly SyncFeedLock SyncFeedLock = new SyncFeedLock();

        public SynchronizeService(ILogger logger, IFeedStorage feedStorage, ISynchonizationStatusManager synchonizationStatusManager, IFeedDbService feedDbService)
        {
            Logger = logger;
            _feedStorage = feedStorage;
            _synchonizationStatusManager = synchonizationStatusManager;
            _feedDbService = feedDbService;
        }

        /// <summary>
        /// Start synchonization job for specified collection
        /// </summary>
        /// <param name="collectionType">Type of collection which should be synchonized</param>
        /// <returns></returns>
        public async Task Synchronize(FeedSyncInfoType collectionType)
        {
            if (!SyncFeedLock.TryLock(collectionType))
            {
                Logger.Info($"Feed sync {collectionType}: can't start because another process is still processed");
                return;
            }

            var lastSynchonizationStatus = _synchonizationStatusManager.GetLast(collectionType, new[] { FeedSyncInfoStatus.Success, FeedSyncInfoStatus.Faild });

            Logger.Info($"Feed sync {collectionType}: last update time - {lastSynchonizationStatus.UpdateBeginTime}");

            var currentSynchonizationStatus = _synchonizationStatusManager.New(lastSynchonizationStatus);

            try
            {
                await SynchronizeCollection(new SyncFeedRequest(lastSynchonizationStatus), currentSynchonizationStatus, collectionType);
            }
            catch (Exception ex)
            {
                Logger.Error($"Feed sync FAILED, items processed so far {currentSynchonizationStatus.ItemsProcessed}. Cause: {ex}");

                _synchonizationStatusManager.Faild(currentSynchonizationStatus);

                throw;
            }
            finally
            {
                SyncFeedLock.Release(collectionType);
            }

            _synchonizationStatusManager.Success(currentSynchonizationStatus);
        }

        private async Task SynchronizeCollection(SyncFeedRequest syncRequest, SynchonizationStatus currentUpdateInfo, FeedSyncInfoType collectionType)
        {
            do
            {
                switch (collectionType)
                {
                    case FeedSyncInfoType.Properties:
                        syncRequest.PageToken = await MoveChunkOfData<FeedVillaModel>(syncRequest, currentUpdateInfo);
                        break;
                    case FeedSyncInfoType.Availabilities:
                        syncRequest.PageToken = await MoveChunkOfData<FeedAvailabilityDiaryModel>(syncRequest, currentUpdateInfo);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(collectionType), collectionType, null);
                }
                Logger.Info($"Feed sync, items processed so far {currentUpdateInfo.ItemsProcessed}, page {syncRequest.PageToken}");

                _synchonizationStatusManager.Update(currentUpdateInfo);
            } while (syncRequest.PageToken != null);
        }

        private async Task<string> MoveChunkOfData<T>(SyncFeedRequest syncRequest, SynchonizationStatus currentUpdateInfo) where T : FeedDocument
        {
            var chunkPropertiesFromDb = _feedDbService.GetChunkFromDb<T>(syncRequest);

            await _feedStorage.UpdateStorage(chunkPropertiesFromDb.Items, syncRequest.IsInitial);

            currentUpdateInfo.ItemsProcessed += chunkPropertiesFromDb.Items.Count;
            currentUpdateInfo.LastPageToken = chunkPropertiesFromDb.NextPageToken;

            return chunkPropertiesFromDb.NextPageToken;
        }

        public void Dispose()
        {
            Logger.Info("Feed sync - process terminated.");
        }
    }
}