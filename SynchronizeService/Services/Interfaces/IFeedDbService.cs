using SynchronizeService.Models;

namespace SynchronizeService.Services.Interfaces
{
    /// <summary>
    /// Service for fetching entries from main DB
    /// </summary>
    public interface IFeedDbService
    {
        /// <summary>
        /// Basing on syncRequest fetch current chunk of items of type T from main DB
        /// </summary>
        /// <typeparam name="T">Type of entry</typeparam>
        /// <param name="syncRequest">Contain needed information to obtain current chunk of data</param>
        /// <returns></returns>
        SyncFeedPageModel<T> GetChunkFromDb<T>(SyncFeedRequest syncRequest) where T : FeedDocument;
    }
}