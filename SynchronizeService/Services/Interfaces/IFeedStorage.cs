using System.Collections.Generic;
using System.Threading.Tasks;
using SynchronizeService.Models;

namespace SynchronizeService.Services.Interfaces
{
    /// <summary>
    /// Service for saving entries in secondary storage
    /// </summary>
    public interface IFeedStorage
    {
        /// <summary>
        /// Save item to storage
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">List of entries to update or add</param>
        /// <param name="assumeAllNew">If True then add item without chking if item exists</param>
        /// <param name="updateOnly">If True then only update item if exist</param>
        /// <returns></returns>
        Task UpdateStorage<T>(List<T> items, bool assumeAllNew = false, bool updateOnly = false) where T : FeedDocument;
    }
}