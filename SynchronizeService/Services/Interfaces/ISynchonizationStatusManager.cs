using SynchronizeService.Models;
using SynchronizeService.Models.Enums;

namespace SynchronizeService.Services.Interfaces
{
    /// <summary>
    /// Service for synchonization history 
    /// </summary>
    public interface ISynchonizationStatusManager
    {
        /// <summary>
        /// Return the last history entry for specified collection type with specified statuses
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="statuses"></param>
        /// <returns></returns>
        SynchonizationStatus GetLast(FeedSyncInfoType collectionType, FeedSyncInfoStatus[] statuses);
        /// <summary>
        /// Save changes in history entry
        /// </summary>
        /// <param name="currentUpdateInfo"></param>
        void Update(SynchonizationStatus currentUpdateInfo);
        /// <summary>
        /// Insert new entry to the history
        /// </summary>
        /// <param name="lastSuccessUpdateInfo"></param>
        /// <returns></returns>
        SynchonizationStatus New(SynchonizationStatus lastSuccessUpdateInfo);
        /// <summary>
        /// Set status to Faild and current date for given entry
        /// </summary>
        /// <param name="currentUpdateInfo"></param>
        void Faild(SynchonizationStatus currentUpdateInfo);
        /// <summary>
        /// Set status to Success and current date for given entry
        /// </summary>
        /// <param name="currentUpdateInfo"></param>
        void Success(SynchonizationStatus currentUpdateInfo);
    }
}