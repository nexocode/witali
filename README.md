Synchronization Service 
=========

This is simplified code for Synchronization Service Job. This job is intended to be used within some schedulling tool like [HangFire](https://www.hangfire.io) or [Quartz](https://www.quartz-scheduler.net/). 

Service will sychnronize secondary storage like NoSQL (feed) with main database (source of truth).

At first (intitial) run job will fetch all entries, but during next synchornization it will fetch only changed entries from the last run.

**Features:**

* Guarantee that only single job is running at same time to avoid conflicts
* Update only changed entries during next synchonization to save resources and time
* Track history of synchonizations
* Continue from the same place in case of failure
* Flexible interfaces allow to implement service for any database or NoSQL storage
* Async methods


### Getting started ###

Implement all needed services: IFeedDbService, IFeedStorage, ISynchonizationStatusManager regarding to your requirements.

```csharp
var syncService = new SynchronizeService(YourImplementationOfFavouriteLogger,
										YourImplementationOfFeedStorage,
										YourImplementationOfSynchonizationStatusManager,
										YourImplementationOfFeedDbService);
await syncService.Synchronize(FeedSyncInfoType.Properties);
```